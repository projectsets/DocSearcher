package org.jab.docsearch.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class at Utils
 *
 * @author henschel
 * @version $Id: UtilsTest.java 230 2018-09-14 14:48:41Z henschel $
 */
public class UtilsTest {

    @Test
    public void testReplaceAll() {
        // once
        assertEquals("&amp;", Utils.replaceAll("&", "&", "&amp;"));
        assertEquals("&", Utils.replaceAll("&amp;", "&amp;", "&"));

        // begin
        assertEquals("&amp;--------", Utils.replaceAll("&", "&--------", "&amp;"));
        assertEquals("&--------", Utils.replaceAll("&amp;", "&amp;--------", "&"));

        // middle
        assertEquals("----&amp;----", Utils.replaceAll("&", "----&----", "&amp;"));
        assertEquals("----&----", Utils.replaceAll("&amp;", "----&amp;----", "&"));

        // end
        assertEquals("--------&amp;", Utils.replaceAll("&", "--------&", "&amp;"));
        assertEquals("--------&", Utils.replaceAll("&amp;", "--------&amp;", "&"));
    }


    @Test
    public void testConvertTextToHTML() {
        assertEquals(null, Utils.convertTextToHTML(null));
        assertEquals("&amp;&amp;", Utils.convertTextToHTML("&&"));
        assertEquals("&nbsp;", Utils.convertTextToHTML("\n"));
        assertEquals("&lt;", Utils.convertTextToHTML("<"));
        assertEquals("&gt;", Utils.convertTextToHTML(">"));
        assertEquals("&quot;", Utils.convertTextToHTML("\""));
    }


    @Test
    public void testGetBaseURLFolder() {
        assertEquals(null, Utils.getBaseURLFolder(null));
        assertEquals(" ", Utils.getBaseURLFolder(" "));
        assertEquals("Hello", Utils.getBaseURLFolder("Hello"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de/"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de/index.html"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de/test"));
        assertEquals("http://www.docsearcher.de/test/", Utils.getBaseURLFolder("http://www.docsearcher.de/test/"));
        assertEquals("http://www.docsearcher.de/test/", Utils.getBaseURLFolder("http://www.docsearcher.de/test/index.html"));
    }


    @Test
    public void testGetDomainURL() {
        assertEquals("", Utils.getDomainURL(null));
        assertEquals("", Utils.getDomainURL(""));
        assertEquals("", Utils.getDomainURL("Hello"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/index.html"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/test"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/test/"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/test/index.html"));
    }
}
