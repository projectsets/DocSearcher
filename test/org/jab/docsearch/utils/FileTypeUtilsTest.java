package org.jab.docsearch.utils;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test class at FileTypeUtils
 *
 * @author henschel
 * @version $Id: FileTypeUtilsTest.java 200 2016-12-05 16:55:14Z henschel $
 */
public class FileTypeUtilsTest {

    @Test
    public void testIsFileTypeText() {
        // not text
        assertFalse(FileTypeUtils.isFileTypeText("filename"));
        assertFalse(FileTypeUtils.isFileTypeText("filename.abc"));

        // text"
        assertTrue(FileTypeUtils.isFileTypeText("filename.txt"));
        assertTrue(FileTypeUtils.isFileTypeText("FILENAME.TXT"));
    }


    @Test
    public void testIsFileTypeHTML() {
        // not text
        assertFalse(FileTypeUtils.isFileTypeHTML("filename"));
        assertFalse(FileTypeUtils.isFileTypeHTML("filename.abc"));

        // text"
        assertTrue(FileTypeUtils.isFileTypeHTML("filename.html"));
        assertTrue(FileTypeUtils.isFileTypeHTML("FILENAME.HTML"));
    }
}
