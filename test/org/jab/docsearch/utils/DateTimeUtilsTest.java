package org.jab.docsearch.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class at DateTimeUtils
 *
 * @author henschel
 * @version $Id: DateTimeUtilsTest.java 200 2016-12-05 16:55:14Z henschel $
 */
public class DateTimeUtilsTest {

    @Test
    public void testGetDateFromString() {
        // null
        assertEquals(null, DateTimeUtils.getDateFromString(null));

        // empty
        assertEquals(null, DateTimeUtils.getDateFromString(""));
        assertEquals(null, DateTimeUtils.getDateFromString(" "));

        // error
        assertEquals(null, DateTimeUtils.getDateFromString("hallo"));
        assertEquals(null, DateTimeUtils.getDateFromString("1-9-1926"));

        // correct date
        assertEquals("Sat Jan 09 00:00:00 CET 1926", DateTimeUtils.getDateFromString("1/9/1926").toString());
        assertEquals("Tue Jan 19 00:00:00 CET 1926", DateTimeUtils.getDateFromString("1/19/1926").toString());
        assertEquals("Tue Oct 19 00:00:00 CET 1926", DateTimeUtils.getDateFromString("10/19/1926").toString());
        assertEquals("Tue Oct 19 00:00:00 CET 1926", DateTimeUtils.getDateFromString("10/19/1926 Hello").toString());
    }
}
