package org.jab.docsearch.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.hslf.exceptions.OldPowerPointFormatException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test class at PowerPoint
 *
 * @author henschel
 * @version $Id: PowerPointTest.java 200 2016-12-05 16:55:14Z henschel $
 */
public class PowerPointTest {

    private PowerPoint converter;


    @BeforeClass
    public static void setUpClass() {
        BasicConfigurator.resetConfiguration();
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.WARN);
    }


    @Test
    public void testParseWithProblem() {

        // null
        converter = new PowerPoint(null);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // unknown file
        converter = new PowerPoint("foo.ppt");
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // PowerPoint5 file
        String file = getClass().getResource("/powerpoint-5-oxp.ppt").getFile();
        converter = new PowerPoint(file);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // check for OldPowerPointFormatException
            assertTrue("OldPowerPointFormatException", ce.getCause() instanceof OldPowerPointFormatException);
            ce.printStackTrace();
        }
    }


    @Test
    public void testParsePowerpoint7_PPT_oxp()
            throws ConverterException {
        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-oxp.ppt").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("Title", "This is test title ", converter.getTitle());
        assertEquals("Author", "Olivier Descout", converter.getAuthor());
        assertEquals("Keywords", null, converter.getKeywords());
        assertEquals("Summary", "This is test title \nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("Text", "This is test title \nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    public void testParsePowerpoint7_PPS_oxp()
            throws ConverterException {
        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-oxp.pps").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("Title", "This is test title ", converter.getTitle());
        assertEquals("Author", "Olivier Descout", converter.getAuthor());
        assertEquals("Keywords", null, converter.getKeywords());
        assertEquals("Summary", "This is test title \nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("Text", "This is test title \nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    public void testParsePowerpoint7_PPT_o2010()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-o2010.ppt").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("Title", "This is test title", converter.getTitle());
        assertEquals("Author", "Olivier Descout", converter.getAuthor());
        assertEquals("Keywords", "JUnit", converter.getKeywords());
        assertEquals("Summary", "This is test title\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("Text", "This is test title\nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    public void testParsePowerpoint7_PPS_o2010()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-o2010.pps").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("Title", "This is test title", converter.getTitle());
        assertEquals("Author", "Olivier Descout", converter.getAuthor());
        assertEquals("Keywords", "JUnit", converter.getKeywords());
        assertEquals("Summary", "This is test title\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("Text", "This is test title\nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    public void testParsePowerpointOOXML_PPT_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-ooxml-o2007.pptx").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("Title", "This is test title", converter.getTitle());
        assertEquals("Author", "Olivier Descout", converter.getAuthor());
        assertEquals("Keywords", null, converter.getKeywords());
        assertEquals("Summary", "This is test title\n\nTest\nThis is test content.\nThat is second test content.\n", converter.getSummary());
        assertEquals("Text", "This is test title\n\nTest\nThis is test content.\nThat is second test content.\n", converter.getText());
    }


    @Test
    public void testParsePowerpointOOXML_PPS_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-ooxml-o2007.ppsx").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("Title", "This is test title", converter.getTitle());
        assertEquals("Author", "Olivier Descout", converter.getAuthor());
        assertEquals("Keywords", null, converter.getKeywords());
        assertEquals("Summary", "This is test title\n\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("Text", "This is test title\n\nTest\nThis is test content.\n", converter.getText());
    }
}
