/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.utils;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

/**
 * This class contains useful methods for working with Lucene.
 *
 * @version $Id: LuceneUtils.java 213 2017-07-19 16:16:19Z henschel $
 */
public class LuceneUtils {

    /**
     * Lucene compatibility version
     */
    public final static Version LUCENE_VERSION = Version.LUCENE_31;

    /**
     * Gets the Lucene index directory.
     *
     * @param path  Path of Lucene index directory
     * @return Lucene Directory
     * @throws IOException
     */
    public static Directory getDirectory(final String path) throws IOException {
        return getDirectory(new File(path));
    }


    /**
     * Gets the Lucene index directory.
     *
     * @param path  Path of Lucene index directory
     * @return Lucene Directory
     * @throws IOException
     */
    public static Directory getDirectory(final File path) throws IOException {
        return new SimpleFSDirectory(path);
    }


    /**
     * Gets the Lucene analyzer.
     *
     * @return Lucene Analyzer
     */
    private static Analyzer getAnalyzer() {
        return new StandardAnalyzer(LUCENE_VERSION);
    }


    /**
     * Gets the Lucene query parser.
     *
     * @param defaultSearchField  The default search field name
     * @return  Lucene query parser
     */
    public static QueryParser getQueryParser(final String defaultSearchField) {
        return new QueryParser(LUCENE_VERSION, defaultSearchField, getAnalyzer());
    }


    /**
     * Get the Lucene index writer.
     *
     * @param directory  Lucene index directory
     * @param create     Create a new index
     * @return Lucene index writer
     * @throws IOException
     */
    public static IndexWriter getIndexWriter(final Directory directory, final boolean create) throws IOException {
    
        // index writer config
        IndexWriterConfig iwc = new IndexWriterConfig(LUCENE_VERSION, getAnalyzer());

        // FIXME Is this really necessary, or the default (create_and_append) enough?
        // new or existing index
        if (create) {
            iwc.setOpenMode(OpenMode.CREATE);
        }
        else {
            iwc.setOpenMode(OpenMode.APPEND);
        }

        // index writer
        IndexWriter iw = new IndexWriter(directory, iwc);

        // ready
        return iw;
    }


    /**
     * Get the Lucene index reader.
     * 
     * @param directory  Lucene index directory
     * @return Lucene index reader.
     * @throws IOException
     */
    public static IndexReader getIndexReader(final Directory directory) throws IOException {
        return IndexReader.open(directory);
    }
}
