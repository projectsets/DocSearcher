/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.constants;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * This enum contains all filetypes.
 *
 * @version $Id: FileType.java 218 2017-09-19 10:56:16Z henschel $
 */
public enum FileType {

	// unknown
	UNKNOWN(null, null),

	// HTML
	HTML("ft_html.png", "html htm shtml shtm mhtml mht xhtml xhtm php asp jsp"),

	// text
	// FIXME Is is better to split text in text and sourcecode?
	TEXT("ft_text.png", "txt csv java py rst md"),

	// MS Word
	MS_WORD("ft_word.png", "doc docx docm dot dotx dotm"),

	// MS Excel
	MS_EXCEL("ft_excel.png", "xls xlsx xlsm"),

	// MS PowerPoint
	MS_POWERPOINT("ft_powerpoint.png", "ppt pps pptx ppsx"),

	// PDF
	PDF("ft_pdf.png", "pdf"),

	// RTF
	RTF("ft_rtf.png", "rtf"),

	// StarOffice/OpenOffice Writer
	OO_WRITER("ft_oowriter.png", "sxw"),

	// StarOffice/OpenOffice Impress
	OO_IMPRESS("ft_ooimpress.png", "sxi sxp"),

	// StarOffice/OpenOffice Calc
	OO_CALC("ft_oocalc.png", "sxc"),

	// StarOffice/OpenOffice Draw
	OO_DRAW("ft_oodraw.png", "sxd"),

	// OpenDocument text
	OPENDOCUMENT_TEXT("ft_oowriter.png", "odt ott odm"),

	// OpenDocument spreadsheet
	OPENDOCUMENT_SPREADSHEET("ft_oocalc.png", "ods ots"),

	// OpenDocument presentation
	OPENDOCUMENT_PRESENTATION("ft_ooimpress.png", "odp otp"),

	// OpenDocument drawing
	OPENDOCUMENT_DRAW("ft_oodraw.png", "odg otg");

	/**
	 * HashMap with all known filetypes.
	 * It is located in an inner class to make sure FileType enum class is fully initialized
	 * before the file type map starts being initialized.
	 */
	private static class FileTypeMap {
		private final static Map<String, FileType> FILETYPE_MAP = new HashMap<>();

		static {
			for (FileType type : FileType.values()) {
				if (type.suffixes != null) {
					for (String extension : type.suffixes.split(" ")) {
						FILETYPE_MAP.put(extension, type);
					}
				}
			}
		}
	}

	/**
	 * Filetype icon
	 */
	private String icon;

	/**
	 * File suffixes
	 */
	private String suffixes;


	/**
	 * Constructor
	 */
	private FileType(final String icon, final String suffixes) {
		this.icon = icon;
		this.suffixes = suffixes;
	}


	/**
	 * Gets the FileType Enum from filename
	 *
	 * @param fileTypeStr  Filetype string
	 * @return             FileType enum
	 */
	public static FileType fromValue(final String fileTypeStr) {
		FileType result = null;

		// null or empty?
		if (! StringUtils.isBlank(fileTypeStr)) {
			result = FileTypeMap.FILETYPE_MAP.get(fileTypeStr.toLowerCase());
		}

		// return
		return result != null ? result : FileType.UNKNOWN;
	}


	/**
	 * Gets the icon.
	 *
	 * @return  the icon string
	 */
	public String getIcon() {
		return icon;
	}


	/**
	 * Get the suffixes.
	 *
	 * @return the suffixes string
	 */
	public String getSuffixes() {
	    return suffixes;
	}
}
